# Strapi + Postgres 数据库 在 [21云盒子](https://www.21cloudbox.com/) 的示例

这是 [21云盒子](http://www.21cloudbox.com/) 上创建的 [Strapi](https://strapi.io/) 示例。

## 部署
详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-strapi-project-in-production.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-strapi-project-in-production.html)